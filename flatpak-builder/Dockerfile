FROM registry.fedoraproject.org/fedora:latest

RUN dnf -y update && \
    dnf install -y \
        flatpak \
        flatpak-builder \
        bubblewrap \
        git \
        json-glib-devel \
        ostree ostree-libs \
        python-gobject \
        python-ruamel-yaml \
        python3-aiohttp \
        python3-gobject \
        python3-paramiko \
        python3-pyyaml \
        python3-requests \
        # JSON parser used eg. in the CI pipeline of the runtime
        jq \
    && \
    dnf clean all

RUN useradd -d /home/flatpakci/ -u 1000 --user-group --create-home flatpakci

# See https://github.com/flatpak/flatpak/issues/5076
ENV FLATPAK_SYSTEM_HELPER_ON_SESSION=foo
USER flatpakci

RUN flatpak --user remote-add flathub https://dl.flathub.org/repo/flathub.flatpakrepo

# 22.08
# Platform, SDK and WebEngine
RUN flatpak --user install -y org.kde.Platform//5.15-22.08
RUN flatpak --user install -y io.qt.qtwebengine.BaseApp//5.15-22.08
RUN flatpak --user install -y org.kde.Sdk//5.15-22.08

# Extensions for apps
RUN flatpak --user install -y org.freedesktop.Sdk.Extension.rust-stable//22.08  # For angelfish
RUN flatpak --user install -y org.freedesktop.Sdk.Extension.llvm15//22.08 # For Kdenlive
RUN flatpak --user install -y org.freedesktop.Sdk.Extension.golang//22.08 # for qmlkonsole (host-spawn)

# 23.08
# Platform, SDK and WebEngine
RUN flatpak --user install -y org.kde.Platform//5.15-23.08
RUN flatpak --user install -y io.qt.qtwebengine.BaseApp//5.15-23.08
RUN flatpak --user install -y org.kde.Sdk//5.15-23.08

# Extensions for apps
RUN flatpak --user install -y org.freedesktop.Sdk.Extension.rust-stable//23.08  # For angelfish
RUN flatpak --user install -y org.freedesktop.Sdk.Extension.llvm16//23.08 # For Kdenlive
RUN flatpak --user install -y org.freedesktop.Sdk.Extension.golang//23.08 # for qmlkonsole (host-spawn)

# Qt 6
RUN flatpak --user install -y org.kde.Platform//6.6
RUN flatpak --user install -y org.kde.Sdk//6.6
RUN flatpak --user install -y io.qt.qtwebengine.BaseApp//6.6
RUN flatpak --user install -y org.kde.Platform//6.7
RUN flatpak --user install -y org.kde.Sdk//6.7
RUN flatpak --user install -y io.qt.qtwebengine.BaseApp//6.7

# KF6 preview
RUN flatpak --user remote-add kde-runtime-nightly https://cdn.kde.org/flatpak/kde-runtime-nightly/kde-runtime-nightly.flatpakrepo
RUN flatpak --user install -y org.kde.Platform//6.6-kf6preview
RUN flatpak --user install -y org.kde.Sdk//6.6-kf6preview
